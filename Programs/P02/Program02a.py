#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 21 16:46:55 2023

@author: putta
"""

def fn(n):
    if n <= 2:
        return n - 1
    else:
        return fn(n-1) + fn(n-2)

try:
    num = int(input("Enter a number : "))
    if num > 0:
        print(f' fn({num}) = {fn(num)}')
    else:
        print("Input should be greater than 0")
except ValueError:
    print("Try with numeric value")
    
    
"""
Fibonacci with memoization

def fn(n, fibDict):
    if n in fibDict:
        return fibDict[n]
    else:
        fibDict[n] = fn(n-1, fibDict) + fn(n-2, fibDict)
        return fibDict[n]

try:
    fibDict = {1:0 , 2:1}
    num = int(input("Enter a number : "))
    if num > 0:
        print(f' fn({num}) = {fn(num, fibDict)}')
    else:
        print("Input should be greater than 0")
except ValueError:
    print("Try with numeric value")
"""    
